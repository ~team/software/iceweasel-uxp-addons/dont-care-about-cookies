Addon "I don't care about cookies" from Daniel Kladnik.
Get rid of annoying cookie warnings from almost all websites.

Licensed under Simplified-BSD.